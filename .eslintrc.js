module.exports = {
    "extends": "airbnb-base",
    "rules": {
      "no-console": 0,
      "no-underscore-dangle": [2, { "allow": ['_id'] }]
    }
};
