# Getting Started Developing

## Dependencies
1. Node.js Version 7 or greater
2. Angular CLI

*NOTE:* Coming soon will be a grunt dependency, but not using it yet

## Install proper node verision
I recommend using `nvm` if you're not already. See how to install nvm: https://github.com/creationix/nvm#node-version-manager---

Once `nvm` is installed run the following command:
```
nvm install 7 default
```

## Installing Angular CLI
```
npm install -g @angular/cli
```

Now that you've gotten the basic dependencies you'll have to install the npm dependencies. From the root of the project run the following:
```
npm install --unsafe-perm
```

*NOTE:* If any cli commands fail with a permission error try using `sudo`

## Developing in Angular
When working with Angular 2 there are a couple ways you might like to work (depending on the scenario):

1. You simply want to work with the front end UI/UX and don't require any api access
2. You want to run a fully functional development version of the application including api access

### Scenario 1 Setup
To simply work on the front end files in angular you can run the following command:
```
npm run-script ng-serve
```

*NOTE:* This will start a test server and open your browser to `localhost:4200`. Anytime you change a file in the Angular code, the changes will be rebuilt and reflected in your browser

### Scenario 2 Setup
To work with a fully functional version of the angular code you'll need to build a bundle with api access run this command:
```
npm run-script ng-serve-bundled
```

*NOTE:* This will create a packaged bundle and symlink it into the server folder where it will be served as a static asset using the koa server. To update the front end resources you must build the bundle again.

## Development Resources
You can find some suggested dev configurations in the `/dev` folder in the root of this repo. Current resources include:

1. Sublime Text Settings
2. Vim - .vimrc

These files are used to help keep a consistent style while developing code.

## Style Guide
This repo can currently be linted against the AirBnB Style Guide for JS.

```
npm run-script lint
```

__TODO:__ Style guide rules will probably require some discussion to make sure we agree on the style choices

## Other Resources
At this point you should have the basic tools required to begin development. Following is a list of some helpful resources to get started.

1. [Angular Fundamentals](https://angular.io/guide/architecture)
