import { tokenNotExpired } from 'angular2-jwt';

export class AuthService {
  loggedIn() {
    return tokenNotExpired();
  }
}
