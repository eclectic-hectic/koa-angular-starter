import { Component } from '@angular/core';
import { LoginFormComponent } from './Form/Component';
import { UserService } from '../User/Service';

@Component({
  templateUrl: './Component.html',
  styleUrls: ['./Component.css'],
  entryComponents: [LoginFormComponent]
})
export class LoginComponent { }
