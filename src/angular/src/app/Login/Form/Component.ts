import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { LoginFormModel } from './Model';
import { UserService } from '../../User/Service';

@Component({
  selector: 'login-form',
  templateUrl: './Component.html',
  styleUrls: ['./Component.css']
})
export class LoginFormComponent {

  public form: FormGroup;
  public model =  new LoginFormModel();
  public submitted = false;

  constructor (private userService: UserService, private router: Router, private fb: FormBuilder) {
    this.createForm();
  }

  createForm() {
    this.form = this.fb.group({
      email: [this.model.email, Validators.email],
      password: [this.model.password, Validators.required],
    });
  }

  async onSubmit(model: LoginFormModel) {
    this.model = model;
    this.submitted = true;
    const { loggedIn, error } = await this.userService.handleLogin(this.model);
    if (!loggedIn) {
      console.log(error);
    } else {
      this.router.navigate(['/']);
    }
  }

  get diagnostic() { return JSON.stringify(this.form.value); }

}
