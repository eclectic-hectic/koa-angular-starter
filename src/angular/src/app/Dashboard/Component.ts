import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../User/Service';

@Component({
  selector: 'app-root',
  templateUrl: './Component.html',
  styleUrls: ['./Component.css'],
})
export class DashboardComponent {
  public title = 'Koa Angular Starter Client';

  public navLinksLoggedIn: Array<any> = [{
    path: '/',
    label: 'Dashboard'
  }, {
    path: '/settings',
    label: 'Settings'
  }, {
    path: '/api-sample',
    label: 'Api'
  }];

  public navLinksLoggedOut: Array<any> = [{
    path: '/login',
    label: 'Log In'
  }, {
    path: '/register',
    label: 'Register'
  }];

  constructor (private router: Router, private userService: UserService) { }

  handleLogout() {
    this.userService.handleLogout();
    this.router.navigate(['/login']);
  }

  get loggedIn() {
    return this.userService.isLoggedIn();
  }

}
