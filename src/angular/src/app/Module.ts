import 'rxjs/Rx';

import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GlobalModule } from './Global/Module';

// Services
import { AuthService } from './Auth/Service';
import { AuthGuardService } from './Auth/Guard/Service';
import { UserService } from './User/Service';

import { Http, HttpModule, RequestOptions } from '@angular/http';
import { AuthHttp, AuthConfig, JwtHelper } from 'angular2-jwt';

// Styles
import {
  MatSnackBarModule,
  MatButtonModule,
  MatProgressBarModule,
  MatSelectModule,
  MatTabsModule,
  MatFormFieldModule,
  MatInputModule,
  MatSlideToggleModule,
  MatCardModule,
  MatListModule
} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Components
import { DashboardComponent } from './Dashboard/Component';
import { HomeComponent } from './Home/Component';
import { LoginComponent } from './Login/Component';
import { LoginFormComponent } from './Login/Form/Component';
import { RegisterComponent } from './Register/Component';
import { RegisterFormComponent } from './Register/Form/Component';
import { SettingsComponent } from './Settings/Component';
import { ApiComponent } from './Api/Component';
import { PageNotFoundComponent } from './PageNotFound/Component';

export function authHttpServiceFactory(http: Http, options: RequestOptions) {
  return new AuthHttp(new AuthConfig(), http, options);
}

const appRoutes: Routes = [
  { path: '', redirectTo: '/hud', pathMatch: 'full' },
  { path: 'home', component: HomeComponent, canActivate: [AuthGuardService] },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'settings', component: SettingsComponent, canActivate: [AuthGuardService] },
  { path: 'api-sample', component: ApiComponent, canActivate: [AuthGuardService] },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [
    GlobalModule.forRoot(),
    HttpModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true }
    ),
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatButtonModule,
    MatProgressBarModule,
    MatSelectModule,
    MatTabsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSlideToggleModule,
    MatCardModule,
    MatListModule,
    BrowserAnimationsModule,
  ],
	declarations: [
		DashboardComponent,
    LoginComponent,
    LoginFormComponent,
    RegisterComponent,
    RegisterFormComponent,
		HomeComponent,
		SettingsComponent,
		ApiComponent,
		PageNotFoundComponent
	],
	providers: [
    {
      provide: AuthHttp,
      useFactory: authHttpServiceFactory,
      deps: [Http, RequestOptions]
    },
    AuthService,
    AuthGuardService,
    JwtHelper,
    UserService
  ],
	bootstrap: [DashboardComponent]
})
export class AppModule { }
