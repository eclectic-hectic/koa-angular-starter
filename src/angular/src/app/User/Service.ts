import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { UserModel } from './Model';
import { JwtHelper, tokenNotExpired, AuthHttp } from 'angular2-jwt';

@Injectable()
export class UserService {

  constructor (
    public userModel: UserModel,
    private http: Http,
    private authHttp: AuthHttp,
    private jwtHelper: JwtHelper
  ) {
    if (tokenNotExpired()) {
      const token = localStorage.getItem('token');
      this.userModel.import(this.jwtHelper.decodeToken(token));
      this.userModel.isLoggedIn = true;
    }
  }

  async handleRegistration (registrationModel) {
    const response = await this.http.post('/api/register', registrationModel).map(res => res.json()).toPromise();
    return response;
  }

  async handleLogin (loginModel) {
    const response = await this.http.post('/api/login', loginModel).map(res => res.json()).toPromise();
    if (response.success) {
      localStorage.setItem('token', response.token);
      this.userModel.import(this.jwtHelper.decodeToken(response.token));
      this.userModel.isLoggedIn = true;
      return { loggedIn: true, error: null };
    } else {
      return { loggedIn: false, error: response.error };
    }
  }

  handleLogout () {
    this.userModel.isLoggedIn = false;
    localStorage.removeItem('token');
  }

  isLoggedIn() {
    return this.userModel.isLoggedIn;
  }
}
