import { Injectable } from '@angular/core';

@Injectable()
export class UserModel {

  _id: string = '';
  email: string = '';
  password: string = '';
  loggedIn: boolean = false;

  import ( data ) {
    for (var key in data) {
      if( this.hasOwnProperty(key) ){
        this[key] = data[key];
      }
    }
  }

  set isLoggedIn(loggedIn: boolean) {
    this.loggedIn = loggedIn;
  }

  get isLoggedIn() {
    return this.loggedIn;
  }

}
