import { NgModule, ModuleWithProviders } from '@angular/core';
import { UserModel } from '../User/Model';

@NgModule({})
export class GlobalModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: GlobalModule,
      providers: [UserModel]
    };
  }
}
