import { Component, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { FormPasswordValidationUtil } from '../../Form/Password/Validation/Util';
import { Router } from '@angular/router';
import { RegisterFormModel } from './Model';
import { UserService } from '../../User/Service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'register-form',
  templateUrl: './Component.html',
  styleUrls: ['./Component.css']
})

export class RegisterFormComponent {

  public form: FormGroup;
  public model = new RegisterFormModel();
  public submitted: boolean = false;

  constructor (
    private userService: UserService,
    private router: Router,
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
  ) {
    this.importModel();
    this.createForm();
  }

  importModel() {
    try {
      const model = JSON.parse(localStorage.getItem('register.form.model'));
      if( model !== null ) {
        this.model = model;
      }
    } catch (e) { }
  }

  createForm() {
    this.form = this.fb.group({
      email: [this.model.email, Validators.email],
      password: [this.model.password, Validators.required],
      confirmPassword: ['', Validators.required],
      firstName: [this.model.firstName, Validators.required],
      lastName: [this.model.lastName, Validators.required],
      primaryPhone: [this.model.primaryPhone, [Validators.pattern('[0-9]{10}'), Validators.required]],
    }, {
      validator: [FormPasswordValidationUtil.matchPassword]
    });
  }

  async onSubmit(model: RegisterFormModel) {
    this.submitted = true;
    this.model = model;
    localStorage.setItem('register.form.model', JSON.stringify(this.model));
    const name = this.form.get('billingCardHolderName').value;
    try {
      const registration = await this.userService.handleRegistration(this.model);
      this.router.navigate(['/login']);
      localStorage.removeItem('register.form.model');
    } catch(e) {
      const { error: { message, key } } = e.json();
      let snackBarMessage = 'An error occurred';
      let snackBarActionText = 'Retry';
      let snackBarAction = () => {
        console.log('No action...')
      };
      switch (message)
      {
        case 'Duplicate key error':
          snackBarMessage = 'That ' + key + ' is already being used!';
          snackBarActionText = 'Login';
          snackBarAction = () => {
            this.router.navigate(['/login']);
          };
        break;
        default:
          snackBarAction = () => {
            this.onSubmit(model);
          };
      }
      let snackBarRef = this.snackBar.open(snackBarMessage, snackBarActionText, {
        duration: 2000,
      });
      snackBarRef.onAction().subscribe(snackBarAction);
    }
  }

  get diagnostic() { return JSON.stringify(this.form.value); }

}
