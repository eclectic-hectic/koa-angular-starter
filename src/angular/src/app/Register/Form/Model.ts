export class RegisterFormModel {

  constructor (
    public email?: string,
    public password?: string,
    public firstName?: string,
    public lastName?: string,
    public primaryPhone?: string,
  ) { }

}
