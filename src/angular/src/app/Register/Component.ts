import { Component } from '@angular/core';
import { RegisterFormComponent } from './Form/Component';

@Component({
  templateUrl: './Component.html',
  styleUrls: ['./Component.css'],
  entryComponents: [RegisterFormComponent]
})
export class RegisterComponent { }
