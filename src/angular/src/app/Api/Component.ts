import { Component, OnInit } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';


@Component({
  templateUrl: './Component.html',
  styleUrls: ['./Component.css']
})
export class ApiComponent implements OnInit {

	data: Object;

	constructor(private http: AuthHttp){}

	ngOnInit(): void {
		this.http.get('/api').subscribe(data => {
			this.data = JSON.stringify(data);
		});
	}

}
