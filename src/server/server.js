const config = require('config');
const app = require('./app');

app.start(config.server.port);
