module.exports = async (ctx) => {
  const result = await ctx.mongo.db(ctx.mongo.databaseName).collection('users').find().toArray();
  ctx.set('Content-Type', 'application/json');
  ctx.body = result;
};
