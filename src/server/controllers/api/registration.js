const User = require('../../models/User.js');

module.exports = async (ctx) => {
  ctx.set('Content-Type', 'application/json');
  const {
    email,
    password,
    firstName,
    lastName,
    primaryPhone,
  } = ctx.request.body;

  const success = true;

  const user = new User();

  user.set('email', email);
  user.set('password', password);
  user.set('firstName', firstName);
  user.set('lastName', lastName);
  user.set('primaryPhone', primaryPhone, 'US');

  try {
    const userInsert = await ctx.mongo.db(ctx.mongo.databaseName).collection('users').insert(user.export());
    user.set('_id', userInsert.ops[0]._id.toString());
  } catch (e) {
    return ctx.renderError(e);
  }

  ctx.body = { success };

  return null;
};
