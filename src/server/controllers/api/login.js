const User = require('../../models/User.js');

module.exports = async (ctx) => {
  ctx.set('Content-Type', 'application/json');
  const { email, password } = ctx.request.body;
  if (typeof email !== 'undefined' && typeof password !== 'undefined') {
    const users = await ctx.mongo.db(ctx.mongo.databaseName).collection('users').find({ email }).toArray();
    if (users.length === 0 || users.length > 1) {
      ctx.status = 403;
      ctx.body = { error: 'Unable to verify user!' };
    } else {
      const [user] = users;
      if (User.hashPassword(password) === user.password) {
        // authentic
        ctx.jwt.sign(user);
        ctx.body = { success: true, token: ctx.jwt.getToken() };
      } else {
        ctx.status = 403;
        ctx.body = { error: 'Invalid email/password!' };
      }
    }
  } else {
    ctx.status = 403;
    ctx.body = { error: 'Empty email/password!' };
  }
};
