const config = require('config');
const winston = require('winston');

class Logger {
  constructor(level = 'info', customLogger) {
    winston.level = level;
    winston.addColors(config.logging.colors);
    this.logger = customLogger || new (winston.Logger)({
      level,
      levels: config.logging.levels,
      transports: [new (winston.transports.Console)({
        colorize: true,
        prettyPrint: true,
        timestamp: true,
      })],
    });
    return this;
  }
  log(...args) {
    this.logger.log(...args);
    return this;
  }
}

module.exports = Logger;
