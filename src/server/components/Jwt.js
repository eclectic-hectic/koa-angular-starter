const jwt = require('jsonwebtoken');

class Jwt {
  constructor(secret) {
    this.secret = secret;
  }
  sign(data, opts) {
    this.token = jwt.sign(data, this.secret, opts);
    this.decoded = jwt.decode(this.token);
  }
  verify(token, opts) {
    this.decoded = jwt.verify(token, this.secret, opts);
  }
  getToken() {
    return this.token;
  }
  getPayload(key) {
    if (typeof key === 'undefined') {
      return this.decoded;
    }
    return this.decoded[key];
  }
}

module.exports = Jwt;
