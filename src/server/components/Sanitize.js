const validator = require('validator');
const voca = require('voca');
const PhoneNumber = require('awesome-phonenumber');

class Sanitize {
  static formatEmail(value) {
    return validator.normalizeEmail(validator.trim(value));
  }
  static formatName(value) {
    return voca.titleCase(validator.stripLow(validator.trim(value)));
  }
  static formatPhone(value, regionCode) {
    const pn = new PhoneNumber(validator.stripLow(validator.trim(value)), regionCode);
    return pn.getNumber('national');
  }
}

module.exports = Sanitize;
