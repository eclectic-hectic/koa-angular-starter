const config = require('config');
const crypto = require('crypto');
const SuperModel = require('super-model');
const Sanitize = require('../components/Sanitize.js');

class User extends SuperModel {
  constructor(options) {
    super(options);
    this.isDirty = true;
    this.filters = {
      password: this.hashPasswordFilter,
      email: Sanitize.formatEmail,
      firstName: Sanitize.formatName,
      lastName: Sanitize.formatName,
    };
    this.interface = {
      _id: 'string',
      firstName: 'string',
      lastName: 'string',
      email: 'string',
      password: 'string',
      primaryPhone: 'string',
    };
  }
  hashPasswordFilter(val) {
    if (this.isDirty) {
      return User.hashPassword(val);
    }
    return val;
  }
  setPrimaryPhone(val, regionCode = 'US') {
    super.set('primaryPhone', Sanitize.formatPhone(val, regionCode));
  }
  set(key, val, ...opts) {
    switch (key) {
      case 'primaryPhone': this.setPrimaryPhone(val, opts[0]);
        break;
      default: super.set(key, val);
    }
  }
  static hashPassword(val) {
    return crypto.createHmac('sha256', config.auth.secret).update(val).digest('hex');
  }
}

module.exports = User;
