const moment = require('moment');

function AccessLogMiddleware(level = 'info') {
  return async (ctx, next) => {
    const startTime = moment();
    await next();
    const endTime = moment();
    const {
      ip,
      method,
      url,
      status,
      req: {
        socket: {
          bytesWritten,
        },
      },
      header,
    } = ctx;
    const userAgent = header['user-agent'];
    const ms = moment.duration(endTime.diff(startTime)).milliseconds();
    ctx.logger.log(level, '%s - "%s %s HTTP/1.1" %s %s "%s" - %dms', ip, method, url, status, bytesWritten, userAgent, ms);
  };
}

module.exports = AccessLogMiddleware;
