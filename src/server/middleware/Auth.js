function AuthMiddleware(Jwt, secret, Model) {
  return async (ctx, next) => {
    const user = new Model();
    const authHeader = ctx.get('Authorization');
    ctx.jwt = new Jwt(secret);
    ctx.loggedIn = false;
    if (typeof authHeader !== 'undefined') {
      const [authType, authToken] = authHeader.split(' ');
      if (authType === 'Bearer') {
        try {
          ctx.jwt.verify(authToken);
          ctx.loggedIn = true;
        } catch (e) {
          console.log(e);
          ctx.logger.log('debug', ctx.jwt.getPayload(authToken));
          ctx.logger.log('debug', e.message);
          ctx.logger.log('warning', 'Invalid JWT passed! Path: %s, Token: %s', ctx.url, authToken);
        }
        if (ctx.loggedIn) {
          const {
            _id,
            email,
            password,
            firstName,
            lastName,
            primaryPhone,
          } = ctx.jwt.getPayload();
          user.import({
            _id, email, password, firstName, lastName, primaryPhone,
          });
        }
      }
    }
    ctx.user = user;
    ctx.logger.log('debug', 'Authentic Request: %s, User: %j', ctx.loggedIn, ctx.user);
    await next();
  };
}

function AuthController(protectedController) {
  return async (ctx, next) => {
    if (ctx.loggedIn) {
      await protectedController(ctx, next);
    } else {
      ctx.set('Content-Type', 'application/json');
      ctx.status = 403;
      ctx.body = { error: 'Access Denied!' };
    }
  };
}

exports.use = AuthMiddleware;
exports.restrict = AuthController;
