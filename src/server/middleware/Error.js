module.exports = async (ctx, next) => {
  ctx.renderError = (e) => {
    const error = e.message.split(': ');
    const body = { error: { code: 500 } };
    ctx.logger.log('error', e.stack);
    if (e.name === 'MongoError') {
      body.error.type = 'databaseError';
      const [msg, indexError] = error;
      switch (msg) {
        case 'E11000 duplicate key error index':
          body.error.message = 'Duplicate key error';
          body.error.key = (() => {
            const [indexPart] = indexError.split(' ');
            const key = indexPart.split('-').pop();
            return key;
          })();
          body.error.code = 409;
          break;
        default:
          body.error.message = 'Internal Server Error';
      }
    } else {
      body.error.type = 'serverError';
      body.error.message = 'Internal Server Error';
    }
    ctx.body = body;
    ctx.status = body.error.code;
  };
  await next();
};
