const fs = require('fs');
const path = require('path');

const apiController = require('./controllers/api.js');
const registrationController = require('./controllers/api/registration.js');
const loginController = require('./controllers/api/login.js');

exports.api = (router, authMiddleware) => {
  router.post('/api/register', registrationController);
  router.post('/api/login', loginController);
  router.get('/api', authMiddleware.restrict(apiController));
  return router;
};

exports.client = (router, bundlePath) => {
  router.get('/(.*)', (ctx) => {
    ctx.set('Content-Type', 'text/html');
    ctx.body = fs.readFileSync(path.resolve(__dirname, bundlePath, './index.html'));
  });
  return router;
};
