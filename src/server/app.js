const config = require('config');
const path = require('path');

const koaMount = require('koa-mount');
const koaStatic = require('koa-static');
const koaMongo = require('koa-mongo');
const koaBodyParser = require('koa-bodyparser');

const mongoDB = require('mongodb');

const Logger = require('./components/Logger.js');
const Jwt = require('./components/Jwt.js');

const Koa = require('koa');
const KoaRouter = require('koa-router');

const HttpAgent = require('httpagent');

const accessLogMiddleware = require('./middleware/AccessLog.js');
const authMiddleware = require('./middleware/Auth.js');
const errorMiddleware = require('./middleware/Error.js');

const routes = require('./routes.js');

const User = require('./models/User.js');

const app = new Koa();
const server = null;

const logger = new Logger(config.logging.level);

let router = new KoaRouter();
router = routes.api(router, authMiddleware);

if (config.client.bundle) {
  const clientBundle = koaMount('/static', koaStatic(path.resolve(__dirname, config.client.bundle.path), config.client.bundle.opts));
  logger.log('debug', 'Bootstraping client bundle...', JSON.stringify(config.client.bundle));
  app.use(clientBundle);
  router = routes.client(router, config.client.bundle.path);
}

app.use(async (ctx, next) => {
  ctx.logger = logger;
  await next();
});
app.use(errorMiddleware);
app.use(koaBodyParser());
app.use(accessLogMiddleware());
if (config.mongo) {
  app.use(koaMongo(config.mongo));
}
app.use(async (ctx, next) => {
  if (typeof ctx.mongo !== "undefined") {
    ctx.mongo.ObjectId = mongoDB.ObjectId;
  }
  await next();
});
app.use(authMiddleware.use(Jwt, config.auth.secret, User));
app.use(router.routes());

module.exports = new HttpAgent(server, app);
